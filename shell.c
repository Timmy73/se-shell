#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#include "readcmd.h"

#define TRUE  1
#define FALSE 0

#define WRITE 1
#define READ  0

int  count_cmd  (char ***seq); // Compte le nombre de commandes (multi-pipe)

int main(int argc, char **argv){

    while (1) {
        struct cmdline *l; // Structure contenant la commande
        
        int pid = 1, status; // Variables utiles pour les forks
        int current_cmd = 0, nb_pipes; // Variables utiles pour la gestion des commandes

        FILE *file_in, *file_out; // Fichiers pour les redirections respectivement de l'entrée & sortie standard
		
        printf("\nshell> ");
		l = readcmd(); // Récupération de la commande

		if (!l) { // Si pointeur null on quitte
			printf("exit\n"); exit(0);
		}
        if(strcmp("exit",l->seq[0][0]) == 0) { // Si la commande est exit on quitte
            printf("Good Bye !!\n"); exit(0);
        };

        // Si il y a une erreur on l'affiche
		if (l->err) printf("error: %s\n", l->err);

        nb_pipes = count_cmd(l->seq) - 1; // Calcul le nombre de pipes
        int descripteurs[nb_pipes][2]; // Tableaux de descripteurs (n_pipes*2)

        /* Tant que le fork a fonctionné et que nous avons pas atteind la dernière commande
           nous créons les descripteurs et processus associés à chaque commande.*/
        while(pid && current_cmd <= nb_pipes){ 
            if(current_cmd < nb_pipes) 
                if(pipe(descripteurs[current_cmd]) < 0) {
                   perror("shell error : Descriptor creation failed"); exit(-1);
                }

            pid = fork();
            if(pid < 0 ){
                perror("shell error: fork failed\n"); exit(-1);
            }

            if(pid != 0){
                /* Si la commande courante n'est pas la première alors on ferme l'entrée 
                   et la sortie du tube de la commande précédente.*/
                if(current_cmd > 0){ 
                    close(descripteurs[current_cmd - 1][READ]);
                    close(descripteurs[current_cmd - 1][WRITE]);
                }
                current_cmd++;  
            }
        }
        
        /* Le processus fils gère les entrées et sorties.
           Le processus fils execute les commandes. */
        if(pid == 0){

            // BLOC TRAITANT LES ENTRÉES DES COMMANDES
            if(current_cmd > 0){
                // Si ce n'est pas la première commande alors on trait le tube de la commande précédente
                close(STDIN_FILENO);
                dup2(descripteurs[current_cmd - 1][READ], STDIN_FILENO);
                close(descripteurs[current_cmd - 1][READ]);
                close(descripteurs[current_cmd - 1][WRITE]);
            }else if(current_cmd == 0){ // Si c'est la première commande
                // Si l'entrée standard est redirigée dans un fichier.
                if(l->in){
                    file_in = fopen(l->in, "r");
                    dup2(fileno(file_in),STDIN_FILENO);
                    fclose(file_in);
                }
            }

            // BLOC TRAITANT LES SORTIES DES COMMANDES
            if(current_cmd < nb_pipes){
                // Si ce n'est pas la dernière commande on traite le tube de la commande courante.
                close(STDOUT_FILENO);
                dup2(descripteurs[current_cmd][WRITE], STDOUT_FILENO);
                close(descripteurs[current_cmd][WRITE]);
                close(descripteurs[current_cmd][READ]);
                
            }else if(current_cmd == nb_pipes){ // Cas de la dernière commande
                if (l->out){ // Si on redirige la sortie standard dans un fichier
                    file_out = fopen(l->out,"w");
                    dup2(fileno(file_out),STDOUT_FILENO);
                    fclose(file_out);
                }
            }

            // Exécution de la commande courrante
            if(execvp(l->seq[current_cmd][0],l->seq[current_cmd]) < 0){
                perror("shell error on execv\n"); exit(-1);
            }

        }else{
            // On attend que chaque processus créé pour chaque commande se termine.
            for(int i = 0; i <= nb_pipes; i++) wait(&status);
        }
	}
    return 0;
}

int count_cmd(char ***seq){
    int i = 0;
    while(seq[i]) i++;
    return i;
}
