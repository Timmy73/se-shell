# Shell

Dans le cadre la 4eme année de la filière INFO de Polytech Grenoble, nous avons réalisé un petit shell (réalisé en C).

Ce shell est capable d'exécuter des commandes, de faire des redirections et d'exécuter des commandes en multi-pipe (`ls | grep sh | wc`).

**Participants :**
* Raphaël AUDIN
* Thomas FRION